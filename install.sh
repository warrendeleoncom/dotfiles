#!/usr/bin/env bash

##############
#### VARS ####
##############

DOTFILES=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
BACKUP="$(dirname "$DOTFILES")/dotfiles_old"

##################
#### FUNCTIONS ###
##################

display_help() {
    echo "Usage: $0 [option]" >&2
    echo
    echo "   -i, --install-apps         Install the must apps"
    echo "   -d, --dotfiles             Install the dotfiles "
    echo
    exit 1
}

function install {
  if [[ "$OSTYPE" == darwin* ]]; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew tap wix/brew
    brew install jq python htop tree wget hh gpg zsh node sublime-text applesimutils watchman 
    brew cask install webstorm postman spotify figma vlc iterm2 google-chrome franz nordvpn skype react-native-debugger hyperswitch istat-menus bartender flipper zsh-syntax-highlighting sourcetree 1Password spectacle adoptopenjdk/openjdk/adoptopenjdk8
    echo "Setting up zsh as a default shell: chsh -s /bin/zsh"
    chsh -s /bin/zsh
  fi
  if [[ "$OSTYPE" == linux* ]]; then
    sudo apt update
    sudo apt install less mc vim htop wget git tree zsh curl dnsutils rar unrar snapd jq 
    sudo usermod --shell /bin/zsh $(whoami)
    sudo snap install node --classic
    sudo snap install prettier --beta
  fi
}

function dotfiles {

  if [[ -d "$DOTFILES" ]]; then
    echo "Symlinking dotfiles from $DOTFILES"
  else
    echo "$DOTFILES does not exist"
    exit 1
  fi

  if [ ! -d "$BACKUP" ]; then
    echo "Creating the backup dotfiles dir: $BACKUP ... "
    mkdir $BACKUP
    echo "Done"
  fi

  if [ -d "~/bin" ]; then
    echo "~/bin already exists. Skipping creation"
  else
    echo "Creating ~/bin"
    mkdir ~/bin
  fi

  if [[ "$OSTYPE" == darwin* ]]; then
    lnFile gitconfig gitconfig 
    
    # hh/hstr
    lnFile hh_blacklist hh_blacklist

    # zsh stuff
    lnFile zsh/zshrc zshrc
  fi
  if [[ "$OSTYPE" == linux* ]]; then
    lnFile gitconfig gitconfig 
    
    # zsh stuff
    lnFile zsh/zshrc zshrc
  fi
}

function lnFile {
  if [ -f "$HOME/.$2" ]; then
     BackupFile "$1"
  fi
  echo "Linking '$DOTFILES/$1' to '$HOME/.$2"
  ln -s $DOTFILES/$1 $HOME/."$2"
  echo -e '\E[0;32m'"Done\033[0m"
}

function BackupFile {
   mv $HOME/.$1 $BACKUP/$1
}

case $1 in
  -i|--install)
    echo "Installing the must apps"
    install
  ;;
  -e|--install-extras)
    echo "Installing the extra-apps"
    install
  ;;
  -d|--dotfiles)
    echo "Symlinking dotfiles"
    dotfiles
  ;;
  *)
    display_help
  ;;
esac
